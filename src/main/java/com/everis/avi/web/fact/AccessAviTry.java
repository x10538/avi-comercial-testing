package com.everis.avi.web.fact;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.facts.Fact;

public class AccessAviTry implements Fact {

    public static AccessAviTry toAviTry() {
        return new AccessAviTry();
    }

    public String toString() {
        return "AccessAviTry{ WEB AVI }";
    }

    @Override
    public void setup(Actor actor) {

    }
}


