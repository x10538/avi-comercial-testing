package com.everis.avi.web.fact;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.facts.Fact;

public class Access implements Fact {

    public static Access toAviTry() {
        return new Access();
    }

    public String toString() {
        return "Access{ WEB AVI }";
    }

    @Override
    public void setup(Actor actor) {

    }
}


