package com.everis.avi.util;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Util {
    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String EliminarCaracteresExtranios(String sTrama){
        //String Regex  = "^([A-Za-z0-9]+)+([A-Za-z]{2,})$";
        String sNuevaTrama = "";
        try{
        sNuevaTrama = sTrama.replaceAll("<p>","").replaceAll("</p>","").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\,", "").replaceAll(" ", "").replaceAll("\n","");
        } catch (Exception e) {
        }
        return sNuevaTrama;
    }

    public String SepararValoresTrama(int iCantidadArreglos, String sCadena, String sCaracter) throws Exception {
        String sCadenaObtenida = "";
        try {
            String[] arrCadenaObtenida;
            arrCadenaObtenida = sCadena.split(sCaracter, -1);
            sCadenaObtenida = arrCadenaObtenida[iCantidadArreglos];
        } catch (Exception e) {
        }
        return sCadenaObtenida;
    }
}
