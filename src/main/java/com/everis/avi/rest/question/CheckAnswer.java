package com.everis.avi.rest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CheckAnswer  implements Question<String> {

    public static Question<String> was() {
        return new CheckAnswer();
    }

    @Override
    public String answeredBy(Actor actor) {
        String path = "sessionCode";
        return SerenityRest.lastResponse().body().path(path);
    }

    public String answeredKey() {
        String path = "sessionCode";
        return SerenityRest.lastResponse().body().path(path);
    }

    public String answeredRespuesta() {
        String path = "answers.text";

        try {
            return SerenityRest.lastResponse().getBody().jsonPath().getList(path).toString();
        }
        catch (Exception e) {
        return null;
        }

    }

    public String answeredCodigo() {
        try {
            String path = "answers.code";// $.answers..code
            return SerenityRest.lastResponse().getBody().jsonPath().getList(path).toString();
        }catch (Exception e)
        {
            return null;
        }
    }
}