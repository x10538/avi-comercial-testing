package com.everis.avi.rest.fact;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.facts.Fact;

public class Access implements Fact {
    public static Access toAvi() {
        return new Access();
    }

    public String toString() {
        return "Access{ API AVI }";
    }

    @Override
    public void setup(Actor actor) {
    }
}
