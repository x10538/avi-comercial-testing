package com.everis.avi.rest.steps;
import com.everis.avi.objetos.ejecucion;
import com.everis.avi.rest.fact.Access;
import com.everis.avi.util.Conexion;
import com.everis.avi.util.FuncionesServicios;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class DialogueService {
    int iCodigoChatBot = 0;
    int iCodigoDiccionario = 0;
    int iCodigoEjecucion = 0;
    String iUsuario = "";
    String sIndidadorWS = "";
    String sIndicadorTipoValidacion = "";
    List<ejecucion> Ejecucion = new ArrayList<>();

    @Given("^The client starts the conversation through the services$")
    public void theClientStartsTheConversationThroughTheServices() throws SQLException {
        theActorCalled("Santiago").has(Access.toAvi());
        Ejecucion = new Conexion().ObtenerDatosCofiguracion();
        if (Ejecucion.size() == 1) {
            for (ejecucion element : Ejecucion) {
                iCodigoEjecucion = element.getiCodigoEjecucion();
                iCodigoChatBot = element.getiCodigoChatBot();
                iCodigoDiccionario = element.getiCodigoDiccionario();
                iUsuario = element.getsUsuario();
                sIndidadorWS = element.getsIndicadorWs();
                sIndicadorTipoValidacion = element.getcIndicadorTipoComparacion();
            }
            new Conexion().ResgistrarDatosTemporal(iCodigoChatBot, iCodigoDiccionario);
        }
    }

    @When("^Get the first answer you will get the session and continue with the dictionary flow$")
    public void getTheFirstAnswerYouWillGetTheSessionAndContinueWithTheDictionaryFlow() throws SQLException {
        if (Ejecucion.size() == 1) {
            if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionWsCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionWsText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            }
        } else {
            new FuncionesServicios().RealizarDialogoSerevicios();
        }

    }

    @When("^Get the first answer you will get the session and continue with the dictionary flow on env$")
    public void getTheFirstAnswerYouWillGetTheSessionAndContinueWithTheDictionaryFlowENV() throws SQLException {
        if (Ejecucion.size() == 1) {
            if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionWsCodeENV(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionWsTextENV(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                new FuncionesServicios().ResalizarConversacionWsCodeENV(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                new FuncionesServicios().ResalizarConversacionWsTextENV(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
            }
        } else {
            new FuncionesServicios().RealizarDialogoSerevicios();
        }

    }

 /*   @When("^Get the first answer you will get the session and continue with the dictionary flow in enviroment {String}$")
    public void getTheFirstAnswerYouWillGetTheSessionAndContinueWithTheDictionaryFlow(String enviroment) throws SQLException {

       if(enviroment.equals("UAT")) {
           if (Ejecucion.size() == 1) {
               if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                   new FuncionesServicios().ResalizarConversacionWsCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
               } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                   new FuncionesServicios().ResalizarConversacionWsText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
               } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("CODE")) {
                   new FuncionesServicios().ResalizarConversacionCode(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
               } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && sIndicadorTipoValidacion.toString().trim().equalsIgnoreCase("TEXT")) {
                   new FuncionesServicios().ResalizarConversacionText(iCodigoChatBot, iCodigoDiccionario, iCodigoEjecucion);
               }
           } else {
               new FuncionesServicios().RealizarDialogoSerevicios();
           }
       }
        if(enviroment.equals("DEV")) {

        }
    }*/

    @Then("^the response obtained with the dictionary will be verified$")
    public void theResponseObtainedWithTheDictionaryWillBeVerified() {
        if (Ejecucion.size() == 1) {
            new Conexion().ResgistrarDatosHistorico(iCodigoEjecucion);
            new Conexion().EliminarDatosTemporal();
            new Conexion().ReiniciarCampoIdentity();
        }
    }

    @And("clean the database")
    public void cleanTheDatabase() {

    }

   /* @When("^choose the desired envoriment to run all tests (.*)$")
    public void chooseTheDesiredEnvorimentToRunAllTests(String env) {
         Serenity.setSessionVariable("enviromentRun").to(env);


        if (env.equals("DEV")) {
            Serenity.setSessionVariable("userRef").to("51999999999");
            Serenity.setSessionVariable("keyHeader").to("688d57cc3d7e47e2a1e9b5b0393c84db");
        }

        if (env.equals("UAT")) {
            Serenity.setSessionVariable("userRef").to("51987659101");
            Serenity.setSessionVariable("keyHeader").to("62b5f305570c41aeb8b90f40e1b1f3e6");

        }
        if (env.equals("UAT2")) {
            Serenity.setSessionVariable("userRef").to("51987659101");
            Serenity.setSessionVariable("keyHeader").to("c9664a560c184e8cb857e5d2a7efabc4");
        }
        if (env.equals("STG")) {
            Serenity.setSessionVariable("userRef").to("51987659101");
            Serenity.setSessionVariable("keyHeader").to("534cbfdafd7b438ba07cbaf250ff094d");

        }


    }*/
}
