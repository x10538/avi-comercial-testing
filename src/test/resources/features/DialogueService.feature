@Servicio
Feature: Conduct a dialogue with the avi service

  Scenario: Run the dialogue service and verify that it responds correctly
    Given The client starts the conversation through the services
    When Get the first answer you will get the session and continue with the dictionary flow
    Then the response obtained with the dictionary will be verified
    And clean the database