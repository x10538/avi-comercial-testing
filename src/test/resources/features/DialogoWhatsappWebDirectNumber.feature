@WsWebDirect
Feature: Conduct dialogue through the graphical interface of Watson try

  Scenario: Conduct a dialogue with the watson try graphic interface to verify the response codes
    Given The "Santiago" actor obtains the configuration data for the execution of the dictionaries and enter to Whatsapp Web direct message number
   When Enter to number chat and try and perform the conversion to get the response codes
   Then Save the dialogs and eliminate the temporary work area